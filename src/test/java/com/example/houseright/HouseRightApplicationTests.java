package com.example.houseright;

import com.szhome.remc.ws.client.QueryClient;
import com.szhome.remc.ws.client.commons.Constants;
import com.szhome.remc.ws.client.commons.QueryResultBean;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class HouseRightApplicationTests {

    @Test
    void contextLoads() {

    }


    public static void main(String[] args) {
        // TODO Auto-generated method stub
        // 申明并且定义一个客户端查询对象
        QueryClient client = new QueryClient();

        /**
         * <H>参数说明 按顺序说明
         * <p>
         * 用户名 (测试接口参数未作限制)
         * <p>
         * 密码 (测试接口参数未作限制)
         * <p>
         * 客户端代码 (测试接口参数未作限制)
         * <p>
         * 查询信息代码 (测试接口提供四种类型信息代码<br>
         *
         * test1:实时查询商品房产权信息,
         *
         * test2:实时查询商品房产数量信息,
         *
         * test3: 实时查询预告登记数量信息,
         *
         * test4:实时预告登记（预售合同备案）信息)
         * <p>
         * 业务参数数组 按协商好的顺序组织业务参数数组(测试接口参数未作限制)
         */
        QueryResultBean bean = client.query("WXJJ", "500915", "WXJJ", "getCqzfCount",	new String[] { "411002560817002" });
        //根据宗地号获取项目栋信息
		/*QueryResultBean bean = client.query("WXJJ", "500915", "WXJJ", "GET_BUILDING",	new String[] { "G10211-0385" });*/
        //根据项目栋编号获取分户数据
		/*QueryResultBean bean = client.query("WXJJ", "500915", "WXJJ", "GET_HOUSE",new String[] { "100000002859" });*/
        //根据证件号码和产权证号查询房屋
        //QueryResultBean bean = client.query("WXJJ", "500915", "WXJJ", "GET_HOUSE_REG", new String[] { "411002560817002","","深圳航空城（东部）实业有限公司" });
        // 取得服务端返回的响应代码 查看查询执行情况
        String resResult = bean.getResResult();
        // 结果代码 (现在的代码存放在constants类中
        // 正确返回结果代码为0
        String resInformation = bean.getResInformation();// 结果信息
        System.out.println(resInformation);
        if (resResult.equals(Constants.RES_RESULT_SUC)) {
            /**
             * 返回的实体bean中其他属性说明
             * <p>
             * bean.getDirection();//方向 标示请求(req)或响应(res)
             * <p>
             * bean.getInfoCode();//查询信息代码
             * <p>
             * bean.getInfoName();//查询信息名称
             * <p>
             * bean.getReqSN();//每次请求的唯一标示
             * <p>
             * bean.getResTime();//响应时间
             */
            int resRsCount = bean.getResRsCount();// 返回结果集的级数
            List list = bean.getResultList();// 封装结果集的List对象
            List rList = null;// 最终结果集存放List对象
            Iterator ite = null;
            Map map = null;// 值存放对象
            for (int i = 0; i < resRsCount; i++) {// 循环多级结果集 按顺序提取返回的最终单个结果集
                rList = (List) list.get(i);// 最终的单个结果集list
                if (rList == null) {// 多级结果集中的某一个最终结果集可能为空
                    // return;// 为空处理代码
                }
                // 不为空取值
                ite = rList.iterator();

                JSONArray jsonArray=new JSONArray(rList);
                System.out.println(jsonArray.toString());

                System.out.println("共查询到"+rList.size()+"条数据：");
                while (ite.hasNext()) {
                    map = (HashMap) ite.next();
                    Set set = map.entrySet();
                    Iterator ite1 = set.iterator();
                    int j = 0;
                    while (ite1.hasNext()) {
                        Map.Entry entry = (Map.Entry) ite1.next();
                        System.out.print(entry.getKey().toString()+":"+map.get(entry.getKey().toString())+" ");// 取得键
                        j++;
                    }
                    System.out.println("");
                }
            }
        }
    }



}
