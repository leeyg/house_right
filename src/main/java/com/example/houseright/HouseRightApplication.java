package com.example.houseright;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages = {"com.example.houseright.dao"})
// @SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class HouseRightApplication {

    public static void main(String[] args) {
        SpringApplication.run(HouseRightApplication.class, args);

    }

}
