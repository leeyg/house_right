package com.example.houseright.dao;

import com.example.houseright.model.STATES;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface STATESMapper {
    int deleteByPrimaryKey(Short stateId);

    int insert(STATES record);

    STATES selectByPrimaryKey(Short stateId);

    List<STATES> selectAll();

    int updateByPrimaryKey(STATES record);
}