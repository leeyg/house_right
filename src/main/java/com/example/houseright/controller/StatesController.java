package com.example.houseright.controller;

import com.example.houseright.dao.STATESMapper;
import com.example.houseright.model.STATES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
@RequestMapping("/statesController")
public class StatesController {

    @Autowired
    private STATESMapper statesMapper;

    @RequestMapping(path = {"/getById"})
    public String getById(@RequestParam(value="stateId") Short stateId){
        Logger log=Logger.getLogger(String.valueOf(StatesController.class));
        log.warning("getLogger");
        STATES states = statesMapper.selectByPrimaryKey(stateId);
        return states.toString();
    }

    @RequestMapping(value = "/testApi", method = RequestMethod.GET)
    public String testApi(@RequestParam(value="stateId") Short stateId){

        STATES states = statesMapper.selectByPrimaryKey(stateId);
        return states.toString();
    }

}
