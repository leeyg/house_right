package com.example.houseright.model;

import java.util.Date;


public class STATES {
    private Short stateId;

    private Object owner;

    private Date creationTime;

    private Date closingTime;

    private Short parentStateId;

    private Short lineageName;

    public Short getStateId() {
        return stateId;
    }

    public void setStateId(Short stateId) {
        this.stateId = stateId;
    }

    public Object getOwner() {
        return owner;
    }

    public void setOwner(Object owner) {
        this.owner = owner;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Date closingTime) {
        this.closingTime = closingTime;
    }

    public Short getParentStateId() {
        return parentStateId;
    }

    public void setParentStateId(Short parentStateId) {
        this.parentStateId = parentStateId;
    }

    public Short getLineageName() {
        return lineageName;
    }

    public void setLineageName(Short lineageName) {
        this.lineageName = lineageName;
    }
}